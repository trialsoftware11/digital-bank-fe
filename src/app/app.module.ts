import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { AuthService } from "./services/auth.service";
import { HttpClientModule } from "@angular/common/http";
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RegisterComponent } from './components/register/register.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import {AuthGuard} from "./guards/auth.guard";
import {VendorService} from "./services/vendor.service";
import {UserService} from "./services/user.service";
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ActivateUserComponent } from './components/activate-user/activate-user.component';
import { UsersListComponent } from './components/users/users-list/users-list.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { UserComponent } from './components/users/user/user.component';
import {DepartmentService} from "./services/department.service";
import { VendorListComponent } from './components/vendors/vendor-list/vendor-list.component';
import { VendorComponent } from './components/vendors/vendor/vendor.component';
import { DepartmentListComponent } from './components/departments/department-list/department-list.component';
import { DepartmentComponent } from './components/departments/department/department.component';
import { TableComponentComponent } from './components/lib/table-component/table-component.component';
import { CustomerComponent } from './components/customers/customer/customer.component';
import { CustomerListComponent } from './components/customers/customer-list/customer-list.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import {TransactionService} from "./services/transaction.service";
import {AccountService} from "./services/account.service";
import {EventService} from "./services/event.service";
import {MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AccountComponent } from './components/accounts/account/account.component';
import { AccountListComponent } from './components/accounts/account-list/account-list.component';
import { AssetComponent } from './components/assets/asset/asset.component';
import { AssetListComponent } from './components/assets/asset-list/asset-list.component';
import {AssetService} from "./services/asset.service";
import { InstructionListComponent } from './components/instructions/instruction-list/instruction-list.component';
import { InstructionComponent } from './components/instructions/instruction/instruction.component';
import {InstructionService} from "./services/instruction.service";
import { ParameterListComponent } from './components/parameters/parameter-list/parameter-list.component';
import { ParameterComponent } from './components/parameters/parameter/parameter.component';
import {ParameterService} from "./services/parameter.service";
import { CommandListComponent } from './components/commands/command-list/command-list.component';
import { CommandComponent } from './components/commands/command/command.component';
import {CommandService} from "./services/command.service";
import { MenuComponent } from './components/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ActivateUserComponent,
    UsersListComponent,
    UserComponent,
    VendorListComponent,
    VendorComponent,
    DepartmentListComponent,
    DepartmentComponent,
    TableComponentComponent,
    CustomerComponent,
    CustomerListComponent,
    TransactionsComponent,
    AccountComponent,
    AccountListComponent,
    AssetComponent,
    AssetListComponent,
    InstructionListComponent,
    InstructionComponent,
    ParameterListComponent,
    ParameterComponent,
    CommandListComponent,
    CommandComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    BrowserAnimationsModule,
    NgxSmartModalModule.forRoot()
  ],
  providers: [AuthService, AuthGuard, VendorService, UserService, DepartmentService,
              TransactionService, EventService, AccountService,AssetService, InstructionService,
              ParameterService, CommandService],
  bootstrap: [AppComponent]
})
export class AppModule { }
