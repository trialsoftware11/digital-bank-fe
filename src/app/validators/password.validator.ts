import { AbstractControl } from '@angular/forms';

export function ValidatePassword(control: AbstractControl) {
  if (control.parent !== undefined) {
    if (control.value != control.parent.controls["password"].value) {
      return { passwordmatch: true };
    }
  }
  return null;
}
