import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import {Customer} from "../../../models/customer.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {CustomerService} from "../../../services/customer.service";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  @Input()
  customer: Customer;

  @Input()
  action: string = "Add";

  public customerForm:FormGroup;

  @Output()
  customerChange = new EventEmitter();

  constructor(private fb:FormBuilder,private customerService:CustomerService,
              public ngxSmartModalService: NgxSmartModalService) {
    this.customerForm = fb.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'lastName': ['', Validators.compose([Validators.required])],
      'gender': ['', Validators.compose([Validators.required])],
      'dateOfBirth': ['', Validators.compose([Validators.required])],
      'status': [''],
    });
  }

  ngOnInit() {
    if(this.customer == undefined){
      this.customer = new Customer("","");
    }
  }

  save() {
    this.customerService.save(this.customer).subscribe((customer)=>{
      this.customerChange.emit(customer);
      this.ngxSmartModalService.getModal('customerModal').close()
    });
  }
}
