import { Component, OnInit } from '@angular/core';
import {Customer} from "../../../models/customer.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {CustomerService} from "../../../services/customer.service";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css'],
  providers: [CustomerService]
})
export class CustomerListComponent implements OnInit {

  customers: Array<Customer> = [];
  customerToDelete: Customer;
  customerToEdit: Customer;
  action: string;

  selectedCustomer: Customer;

  constructor(private customerService:CustomerService,
              public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.customerService.findAll().subscribe((customerList)=>{
      this.customers = customerList;
    });
  }

  showCustomerDialog(action: string, customer: Customer){
    this.customerToEdit = customer;
    this.action = action;
    this.ngxSmartModalService.getModal('customerModal').open();
  }

  confirmDelete(customer: Customer){
    this.customerToDelete = customer;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.customerService.delete(this.customerToDelete.customerId).subscribe(()=>{
      this.customers = this.customers.filter((customer) => {
        return customer.customerId != this.customerToDelete.customerId;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateCustomerList(customer: Customer){
    if(!this.customers.some(elem => elem.customerId === customer.customerId)){
      this.customers.push(customer);
    }
    this.customerToEdit = new Customer("","");
  }

}
