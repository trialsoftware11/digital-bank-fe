import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-activate-user',
  templateUrl: './activate-user.component.html',
  styleUrls: ['./activate-user.component.css']
})
export class ActivateUserComponent implements OnInit {

  busy: boolean = false;

  constructor(private userService:UserService, private route:ActivatedRoute) { }

  ngOnInit() {
    console.log("Activating user")
    this.busy = true;
    this.route.queryParams.subscribe(params => {
      this.userService.activate(params['tkn']).subscribe((user)=>{
        this.busy = false;
      });
    });
  }

}
