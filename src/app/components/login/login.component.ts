import { Component, OnInit,AfterViewInit } from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {User} from "../../models/user.model";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {ViewChild} from "@angular/core";
import {ElementRef} from "@angular/core";
import {VendorService} from "../../services/vendor.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  public loginFormGroup:FormGroup;
  busy: boolean = false;
  user: User = new User();
  validUser: boolean = false;
  message: string;

  @ViewChild('username') inputEl:ElementRef;

  constructor(private fb:FormBuilder, private authService: AuthService, private vendorService:VendorService, private router: Router) {
    this.loginFormGroup = fb.group({
      'username': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])]
    });

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // setTimeout(() => this.inputEl.nativeElement.focus());
  }

  public authenticate():void {
    if(this.formValid()) {
      this.busy = true;
      this.authService.authenticate(this.user).subscribe((user)=> {
        this.busy = false;
        this.handleResult(user)
      });
    }
  }

  handleResult(user:User[]){
    this.validUser = user['id'] > 0;
    if(this.validUser){
      this.authService.user = (Array.isArray(user))?user[0]:user;

      this.vendorService.findById(user['vendorId']).subscribe((vendor) => {
        this.authService.userVendor = (Array.isArray(vendor))?vendor[0]:vendor;
      });

      this.router.navigate(['/vendor-list'])

    }else{
      this.message = user['message'];
    }
  }

  formValid(): Boolean {
    for(let key of Object.keys(this.loginFormGroup.controls)){
      if(this.loginFormGroup.controls[key].invalid){
        return false;
      }
    }
    return true;
  }
}
