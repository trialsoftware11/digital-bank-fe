import {Component, OnInit} from "@angular/core";
import {User} from "../../models/user.model";
import {FormGroup} from "@angular/forms";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {Validators} from "@angular/forms";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  user:User = new User();
  public forgotPasswordForm:FormGroup;
  busy:boolean = false;
  done:boolean = false;

  constructor(private fb:FormBuilder, private router:Router, private userService:UserService) {
    this.forgotPasswordForm = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'username': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
  }


  home() {
    this.router.navigate(['/login']);
    return false;
  }

  send(){
    this.busy = true;
    this.userService.sendPasswordResetEmail(this.user).subscribe((user)=>{
      this.done = true;
      this.busy = false;
    });
  }

}
