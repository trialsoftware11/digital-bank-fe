import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {ParameterService} from "../../../services/parameter.service";
import {Parameter} from "../../../models/parameter.model";

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.css']
})
export class ParameterComponent implements OnInit {

  @Input()
  parameter: Parameter;

  @Input()
  action: string = "Add";

  public parameterForm:FormGroup;

  @Output()
  parameterChange = new EventEmitter();

  constructor(private fb:FormBuilder,private parameterService:ParameterService,
              public ngxSmartModalService: NgxSmartModalService) {
    this.parameterForm = fb.group({
      'parameterName': ['', Validators.compose([Validators.required])],
      'parameterFieldName': ['', Validators.compose([Validators.required])],
      'parameterValue': ['', Validators.compose([Validators.required])],
      'parameterValueType': ['', Validators.compose([Validators.required])],
      'filterCondition': ['', Validators.compose([Validators.required])],
      'nextJoinType': ['', Validators.compose([Validators.required])],
      'status': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    if(this.parameter == undefined){
      this.parameter = new Parameter();
    }
  }

  save() {
    this.parameterService.save(this.parameter).subscribe((parameter)=>{
      this.parameterChange.emit(parameter);
      this.ngxSmartModalService.getModal('parameterModal').close()
    });
  }

}
