import { Component, OnInit } from '@angular/core';
import {Parameter} from "../../../models/parameter.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {ParameterService} from "../../../services/parameter.service";

@Component({
  selector: 'app-parameter-list',
  templateUrl: './parameter-list.component.html',
  styleUrls: ['./parameter-list.component.css']
})
export class ParameterListComponent implements OnInit {

  parameters: Array<Parameter> = [];
  parameterToDelete: Parameter;
  parameterToEdit: Parameter;
  action: string;

  selectedParameter: Parameter;

  constructor(private parameterService:ParameterService,
              public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.parameterService.findAll().subscribe((parameterList)=>{
      this.parameters = parameterList;
    });
  }

  showParameterDialog(action: string, parameter: Parameter){
    this.parameterToEdit = parameter;
    this.action = action;
    this.ngxSmartModalService.getModal('parameterModal').open();
  }

  confirmDelete(parameter: Parameter){
    this.parameterToDelete = parameter;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.parameterService.delete(this.parameterToDelete.parameterId).subscribe(()=>{
      this.parameters = this.parameters.filter((parameter) => {
        return parameter.parameterId != this.parameterToDelete.parameterId;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateParameterList(parameter: Parameter){
    if(!this.parameters.some(elem => elem.parameterId === parameter.parameterId)){
      this.parameters.push(parameter);
    }
    this.parameterToEdit = new Parameter();
  }

}
