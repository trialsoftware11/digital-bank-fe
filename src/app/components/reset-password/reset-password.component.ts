import {Component, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {FormBuilder} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Validators} from "@angular/forms";
import {ValidatePassword} from "../../validators/password.validator";
import {User} from "../../models/user.model";
import {ActivatedRoute} from "@angular/router";
import {Router} from "@angular/router";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  public resetPasswordForm:FormGroup;
  user:User = new User();
  forgotPasswordToken:string;
  done: boolean = false;
  busy: boolean = false;

  constructor(private fb:FormBuilder, private userService:UserService, private route:ActivatedRoute,
              private router: Router) {
    this.resetPasswordForm = fb.group({
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      'passwordConfirm': ['', Validators.compose([Validators.required, ValidatePassword])],
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.forgotPasswordToken = params['tkn'];
      this.userService.findByToken(this.forgotPasswordToken).subscribe((user)=>{
        this.user = user;
      });
    });
  }

  reset() {
    this.busy = true;
    this.user.forgotPasswordToken = "";
    this.user.forgotPasswordTokenExpiry = "";
    this.userService.save(this.user).subscribe((user)=>{
      this.done = true;
      this.busy = false;
    });
  }

  home() {
    this.router.navigate(['/login']);
    return false;
  }
}
