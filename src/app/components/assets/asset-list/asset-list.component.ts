import { Component, OnInit } from '@angular/core';
import {Asset} from "../../../models/asset.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {AssetService} from "../../../services/asset.service";

@Component({
  selector: 'app-asset-list',
  templateUrl: './asset-list.component.html',
  styleUrls: ['./asset-list.component.css']
})
export class AssetListComponent implements OnInit {

  assets: Array<Asset> = [];
  assetToDelete: Asset;
  assetToEdit: Asset;
  action: string;

  selectedAsset: Asset;

  constructor(private assetService:AssetService,
              public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.assetService.findAll().subscribe((assetList)=>{
      this.assets = assetList;
    });
  }

  showAssetDialog(action: string, asset: Asset){
    this.assetToEdit = asset;
    this.action = action;
    this.ngxSmartModalService.getModal('assetModal').open();
  }

  confirmDelete(asset: Asset){
    this.assetToDelete = asset;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.assetService.delete(this.assetToDelete.assetId).subscribe(()=>{
      this.assets = this.assets.filter((asset) => {
        return asset.assetId != this.assetToDelete.assetId;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateAssetList(asset: Asset){
    if(!this.assets.some(elem => elem.assetId === asset.assetId)){
      this.assets.push(asset);
    }
    this.assetToEdit = new Asset("", 0);
  }


}
