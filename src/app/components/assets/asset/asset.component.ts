import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {AssetService} from "../../../services/asset.service";
import {Asset} from "../../../models/asset.model";


@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.css']
})
export class AssetComponent implements OnInit {

  @Input()
  asset: Asset;

  @Input()
  action: string = "Add";

  public assetForm:FormGroup;

  @Output()
  assetChange = new EventEmitter();

  constructor(private fb:FormBuilder,private assetService:AssetService,
              public ngxSmartModalService: NgxSmartModalService) {
    this.assetForm = fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'assetType': ['', Validators.compose([Validators.required])],
      'dateOfPurchase': ['', Validators.compose([Validators.required])],
      'initialValue': ['', Validators.compose([Validators.required])],
      'valueOwing': ['', Validators.compose([Validators.required])],
      'currentValue': ['', Validators.compose([Validators.required])],
      'marketValue': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    if(this.asset == undefined){
      this.asset = new Asset("",0);
    }
  }

  save() {
    this.assetService.save(this.asset).subscribe((asset)=>{
      this.assetChange.emit(asset);
      this.ngxSmartModalService.getModal('assetModal').close()
    });
  }
}
