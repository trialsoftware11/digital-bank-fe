import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {UserRole} from "../../enums/user.role";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent  {

  currentDate = new Date();

  userRoles: UserRole;

  constructor(private authService: AuthService, private router: Router){

  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/login'])
  }

  navigateTo(url: string){
    this.router.navigate(['/'+url]);
  }

  currentUser(){
    return this.authService.user;
  }

  currentVendor() {
    return this.authService.userVendor;
  }

}
