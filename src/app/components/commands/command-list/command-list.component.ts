import { Component, OnInit } from '@angular/core';
import {Command} from "../../../models/command.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {CommandService} from "../../../services/command.service";

@Component({
  selector: 'app-command-list',
  templateUrl: './command-list.component.html',
  styleUrls: ['./command-list.component.css']
})
export class CommandListComponent implements OnInit {

  commands: Array<Command> = [];
  commandToDelete: Command;
  commandToEdit: Command;
  action: string;

  selectedCommand: Command;

  constructor(private commandService:CommandService,
              public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.commandService.findAll().subscribe((commandList)=>{
      this.commands = commandList;
    });
  }

  showCommandDialog(action: string, command: Command){
    this.commandToEdit = command;
    this.action = action;
    this.ngxSmartModalService.getModal('commandModal').open();
  }

  confirmDelete(command: Command){
    this.commandToDelete = command;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.commandService.delete(this.commandToDelete.commandId).subscribe(()=>{
      this.commands = this.commands.filter((command) => {
        return command.commandId != this.commandToDelete.commandId;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateCommandList(command: Command){
    if(!this.commands.some(elem => elem.commandId === command.commandId)){
      this.commands.push(command);
    }
    this.commandToEdit = new Command();
  }

}
