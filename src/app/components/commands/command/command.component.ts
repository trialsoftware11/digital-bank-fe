import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {CommandService} from "../../../services/command.service";
import {Command} from "../../../models/command.model";

@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
  styleUrls: ['./command.component.css']
})
export class CommandComponent implements OnInit {

  @Input()
  command: Command;

  @Input()
  action: string = "Add";

  public commandForm:FormGroup;

  @Output()
  commandChange = new EventEmitter();

  constructor(private fb:FormBuilder,private commandService:CommandService,
              public ngxSmartModalService: NgxSmartModalService) {
    this.commandForm = fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'commandSource': ['', Validators.compose([Validators.required])],
      'commandCreationDate': ['', Validators.compose([Validators.required])],
      'status': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    if(this.command == undefined){
      this.command = new Command();
    }
  }

  save() {
    this.commandService.save(this.command).subscribe((command)=>{
      this.commandChange.emit(command);
      this.ngxSmartModalService.getModal('commandModal').close()
    });
  }

}
