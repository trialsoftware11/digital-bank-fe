import { Component, OnInit } from '@angular/core';
import {User} from "../../models/user.model";
import {Validators} from "@angular/forms";
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {AbstractControl} from "@angular/forms";
import {ValidatePassword} from "../../validators/password.validator";
import {VendorService} from "../../services/vendor.service";
import {Vendor} from "../../models/vendor.model";
import {Department} from "../../models/department.model";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User = new User();
  public registerForm:FormGroup;
  passwordConfirm: string;
  vendors: Array<Vendor> = [];
  departments: Array<Department> = [];
  busy: boolean = false;
  registrationDone: boolean = false;

  constructor(private fb:FormBuilder, private router: Router, private vendorService: VendorService,
  private userService: UserService) {
    this.registerForm = fb.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'lastName': ['', Validators.compose([Validators.required])],
      'gender': ['', Validators.compose([Validators.required])],
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'username': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      'passwordConfirm': ['', Validators.compose([Validators.required, ValidatePassword])],
    });
  }

  ngOnInit() {
    this.vendorService.findAll().subscribe((vendors) => {
      this.vendors = vendors;
    });
  }

  home() {
    this.router.navigate(['/login']);
    return false;
  }

  loadDepartments(){
    this.departments = this.vendors.filter((vendor) => {
      return vendor.id == this.user.vendorId;
    })[0].departments;
  }

  register(){
    this.busy = true;
    this.userService.register(this.user).subscribe((user) => {
      this.busy = false;
      this.registrationDone = true;
    });
  }

}
