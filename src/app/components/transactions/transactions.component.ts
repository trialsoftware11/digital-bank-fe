import {Component, OnInit} from '@angular/core';
import {TransactionService} from "../../services/transaction.service";
import {Transaction} from "../../models/transaction.model";
import {Event} from "../../models/event.model";
import {EventService} from "../../services/event.service";
import {AccountService} from "../../services/account.service";
import {Account} from "../../models/account.model";
import {Asset} from "../../models/asset.model";
import {AccountType} from "../../enums/account.type";
import {Liability} from "../../models/liability.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  transactions:Array<Transaction> = [];
  events:Array<Event> = [];
  accounts:Array<Account> = [];
  assets:Array<Asset> = [];
  liabilities:Array<Liability> = [];
  totalAssets:number = 0;
  totalLiabilities:number = 0;
  account:Account;

  //Dummy
  savings = 0;
  current = 0;
  totalCash = 0;
  callAccount = 0;
  unitTrust = 0;
  totalInvestments = 0;
  vehicleLoan = 0;
  personalLoan = 9;
  totalLoans = 0;
  residentialLoan = 0;
  totalMortgages = 0;

  constructor(private transactionService:TransactionService,
              private eventService:EventService,
              private accountService:AccountService,
              private router: Router) {
  }

  ngOnInit() {
    this.transactionService.findAll().subscribe((transactions) => {
      this.transactions = transactions;
    });

    this.eventService.findAll().subscribe((events) => {
      this.events = events;
    });

    this.accountService.findByOwnerId(1).subscribe((accounts)=> {
      this.accounts = accounts;
      this.handleAccount();
    });

    this.accountService.findById(2).subscribe((account)=> {
      if (account != undefined) {
        this.account = account;
      }
    })
    // this.loadStaticData()
    this.initialiseFields();

  }

  //TODO - Dummy
  initialiseFields() {
    // const dummyData  = {
    //   "_id": {
    //     "timestamp": 1350508407,
    //     "machineIdentifier": 12384364,
    //     "processIdentifier": -10343,
    //     "counter": 4427793,
    //     "date": "2012-10-17T21:13:27.000+0000",
    //     "time": 1350508407000,
    //     "timeSecond": 1350508407
    //   },
    //   "savings": 1400,
    //   "current": 3625.2,
    //   "totalCash": 0,
    //   "callAccount": 78565,
    //   "unitTrust": 14552,
    //   "totalInvestments": 0,
    //   "vehicleLoan": 524444,
    //   "personalLoan": 150700,
    //   "totalLoans": 0,
    //   "residentialLoan": 1405000,
    //   "totalMortgages": 0
    // };

    this.transactionService.getDummy().subscribe((dummyData) => {
      this.savings = dummyData.savings;
      this.current = dummyData.current;
      this.callAccount = dummyData.callAccount;
      this.unitTrust = dummyData.unitTrust;
      this.vehicleLoan = dummyData.vehicleLoan;
      this.personalLoan = dummyData.personalLoan;
      this.residentialLoan = dummyData.residentialLoan;

      this.totalCash = dummyData.savings + dummyData.current;
      this.totalInvestments = dummyData.callAccount + dummyData.unitTrust;
      this.totalAssets = this.totalCash + this.totalInvestments;

      this.totalLoans = dummyData.vehicleLoan + dummyData.personalLoan;
      this.totalMortgages += dummyData.residentialLoan;

      this.totalLiabilities = this.totalLoans + this.totalMortgages;
    });
  }

  loadStaticData() {
    this.transactions = [
      {
        "transactionId": 4,
        "name": "TR205EDSE",
        "transactionValueType": "_CLASS",
        "targetClassName": null,
        "targetClassFieldName": null,
        "sourceClassName": null,
        "sourceClassFieldName": "STRING_VALUE",
        "sourceValue": "543FESSD"
      },
      {
        "transactionId": 5,
        "name": "TR2L374SE",
        "transactionValueType": "_CLASS",
        "targetClassName": null,
        "targetClassFieldName": null,
        "sourceClassName": null,
        "sourceClassFieldName": "STRING_VALUE",
        "sourceValue": "915D338"
      },
      {
        "transactionId": 6,
        "name": "TR733U636",
        "transactionValueType": "_CLASS",
        "targetClassName": null,
        "targetClassFieldName": null,
        "sourceClassName": null,
        "sourceClassFieldName": "STRING_VALUE",
        "sourceValue": "392U8EHD"
      },
      {
        "transactionId": 7,
        "name": "TR5D4DSF4",
        "transactionValueType": "_CLASS",
        "targetClassName": null,
        "targetClassFieldName": null,
        "sourceClassName": null,
        "sourceClassFieldName": "STRING_VALUE",
        "sourceValue": "O4NPFOD"
      }
    ];

    this.events = [
      {
        "eventId": 9,
        "accountId": 0,
        "assetId": 0,
        "userId": 0,
        "type": "SYSTEM",
        "title": null,
        "description": null,
        "amount": 0,
        "transactionType": "STANDARD",
        "transactionId": 0,
        "creationDate": new Date()
      },
      {
        "eventId": 10,
        "accountId": 0,
        "assetId": 0,
        "userId": 0,
        "type": "TRANSACTION",
        "title": null,
        "description": null,
        "amount": 0,
        "transactionType": "WITHDDRAWAL",
        "transactionId": 0,
        "creationDate": new Date()
      },
      {
        "eventId": 11,
        "accountId": 0,
        "assetId": 0,
        "userId": 0,
        "type": "TRANSACTION",
        "title": null,
        "description": null,
        "amount": 0,
        "transactionType": "TRANSFER",
        "transactionId": 0,
        "creationDate": new Date()
      },
      {
        "eventId": 12,
        "accountId": 0,
        "assetId": 0,
        "userId": 0,
        "type": "CUSTOMER",
        "title": null,
        "description": null,
        "amount": 0,
        "transactionType": "STANDARD",
        "transactionId": 0,
        "creationDate": new Date()
      }
    ];

    this.accounts = [
      {
        "accountId": 2,
        "accountType": AccountType.DEBIT,
        "ownerId": 1,
        "minimumBalance": 0,
        "balance": 54121,
        "lastBalance": 0,
        "score": 786,
        "withdrawalRestriction": 0,
        "withdrawalLimit": 0,
        "overdrawLimit": 0,
        "accrualRate": 0,
        "status": "ACTIVE",
        "owner": {
          "customerId": 1,
          "clientUserId": 0,
          "firstName": "John",
          "lastName": "Bayab",
          "gender": "male",
          "male": true,
          "female": true,
          "married": true,
          "disability": true,
          "selfEmployed": true,
          "lifeInsured": true,
          "medicalAid": true,
          "pension": true,
          "salaryOnDebitOrder": 0,
          "dateOfBirth": Date(),
          "age": 0,
          "positiveBalanceScore": 0,
          "increasingBalanceScore": 0,
          "status": "ACTIVE"
        }
      }
    ];

    this.account = this.accounts[0]


  }

  handleAccount() {
    this.accounts.forEach((account) => {
      switch (account.accountType) {
        case AccountType.INVESTMENT:
          this.addToAssets(new Asset(account.accountType, account.balance));
          break;
        case AccountType.DEBIT:
          this.addToAssets(new Asset(account.accountType, account.balance));
          break;
        case AccountType.CREDIT:
          this.addToLiabilites(new Liability(account.accountType, account.balance));
          break;
        case AccountType.LOAN:
          this.addToLiabilites(new Liability(account.accountType, account.balance));
          break;
        default:
          console.log("unknown account type")
          break;
      }
    });

  }

  addToAssets(asset:Asset) {
    this.totalAssets += asset.currentValue;
    this.assets.push(asset);
  }

  addToLiabilites(liability:Liability) {
    this.totalLiabilities += liability.currentValue;
    this.liabilities.push(liability);
  }

}
