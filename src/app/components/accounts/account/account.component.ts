import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import {Account} from "../../../models/account.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {AccountService} from "../../../services/account.service";
import {Customer} from "../../../models/customer.model";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  @Input()
  account: Account;

  @Input()
  action: string = "Add";

  public accountForm:FormGroup;

  @Output()
  accountChange = new EventEmitter();

  constructor(private fb:FormBuilder,private accountService:AccountService,
              public ngxSmartModalService: NgxSmartModalService) {
    this.accountForm = fb.group({
      'accountType': ['', Validators.compose([Validators.required])],
      'minimumBalance': ['', Validators.compose([Validators.required])],
      'balance': ['', Validators.compose([Validators.required])],
      'lastBalance': ['', Validators.compose([Validators.required])],
      'score': ['', Validators.compose([Validators.required])],
      'withdrawalRestriction': ['', Validators.compose([Validators.required])],
      'withdrawalLimit': ['', Validators.compose([Validators.required])],
      'overdrawLimit': ['', Validators.compose([Validators.required])],
      'accrualRate': ['', Validators.compose([Validators.required])],
      'status': [''],
    });
  }

  ngOnInit() {
    if(this.account == undefined){
      this.account = new Account(0,new Customer('',''),0);
    }
  }

  save() {
    this.accountService.save(this.account).subscribe((account)=>{
      this.accountChange.emit(account);
      this.ngxSmartModalService.getModal('accountModal').close()
    });
  }

}
