import { Component, OnInit } from '@angular/core';
import {Account} from "../../../models/account.model";
import {Customer} from "../../../models/customer.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {AccountService} from "../../../services/account.service";

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {

  accounts: Array<Account> = [];
  accountToDelete: Account;
  accountToEdit: Account;
  action: string;

  selectedAccount: Account;

  constructor(private accountService:AccountService,
              public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.accountService.findAll().subscribe((accountList)=>{
      this.accounts = accountList;
    });
  }

  showAccountDialog(action: string, account: Account){
    this.accountToEdit = account;
    this.action = action;
    this.ngxSmartModalService.getModal('accountModal').open();
  }

  confirmDelete(account: Account){
    this.accountToDelete = account;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.accountService.delete(this.accountToDelete.accountId).subscribe(()=>{
      this.accounts = this.accounts.filter((account) => {
        return account.accountId != this.accountToDelete.accountId;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateAccountList(account: Account){
    if(!this.accounts.some(elem => elem.accountId === account.accountId)){
      this.accounts.push(account);
    }
    this.accountToEdit = new Account(0, new Customer('',''), 0);
  }

}
