import { Component, OnInit } from '@angular/core';
import {Instruction} from "../../../models/instruction.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {InstructionService} from "../../../services/instruction.service";

@Component({
  selector: 'app-instruction-list',
  templateUrl: './instruction-list.component.html',
  styleUrls: ['./instruction-list.component.css']
})
export class InstructionListComponent implements OnInit {

  instructions: Array<Instruction> = [];
  instructionToDelete: Instruction;
  instructionToEdit: Instruction;
  action: string;

  selectedInstruction: Instruction;

  constructor(private instructionService:InstructionService,
              public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.instructionService.findAll().subscribe((instructionList)=>{
      this.instructions = instructionList;
    });
  }

  showInstructionDialog(action: string, instruction: Instruction){
    this.instructionToEdit = instruction;
    this.action = action;
    this.ngxSmartModalService.getModal('instructionModal').open();
  }

  confirmDelete(instruction: Instruction){
    this.instructionToDelete = instruction;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.instructionService.delete(this.instructionToDelete.instructionId).subscribe(()=>{
      this.instructions = this.instructions.filter((instruction) => {
        return instruction.instructionId != this.instructionToDelete.instructionId;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateInstructionList(instruction: Instruction){
    if(!this.instructions.some(elem => elem.instructionId === instruction.instructionId)){
      this.instructions.push(instruction);
    }
    this.instructionToEdit = new Instruction();
  }

}
