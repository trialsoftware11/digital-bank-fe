import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {InstructionService} from "../../../services/instruction.service";
import {Instruction} from "../../../models/instruction.model";

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.css']
})
export class InstructionComponent implements OnInit {

  @Input()
  instruction: Instruction;

  @Input()
  action: string = "Add";

  public instructionForm:FormGroup;

  @Output()
  instructionChange = new EventEmitter();

  constructor(private fb:FormBuilder,private instructionService:InstructionService,
              public ngxSmartModalService: NgxSmartModalService) {
    this.instructionForm = fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'instructionType': ['', Validators.compose([Validators.required])],
      'instructionCreationDate': ['', Validators.compose([Validators.required])],
      'instructionExecutionDate': ['', Validators.compose([Validators.required])],
      'status': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    if(this.instruction == undefined){
      this.instruction = new Instruction();
    }
  }

  save() {
    this.instructionService.save(this.instruction).subscribe((instruction)=>{
      this.instructionChange.emit(instruction);
      this.ngxSmartModalService.getModal('instructionModal').close()
    });
  }

}
