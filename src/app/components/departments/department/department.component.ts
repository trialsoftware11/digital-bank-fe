import { Component, OnInit , Input, Output,  EventEmitter,OnChanges} from '@angular/core';
import {Department} from "../../../models/department.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {DepartmentService} from "../../../services/department.service";
import {Validators} from "@angular/forms";
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {VendorService} from "../../../services/vendor.service";
import {Vendor} from "../../../models/vendor.model";

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  @Input()
  department: Department = new Department();

  public departmentForm:FormGroup;

  @Input()
  action: string = "Add";

  @Output()
  departmentChange = new EventEmitter();

  vendors: Array<Vendor> = new Array<Vendor>();

  constructor(private fb:FormBuilder,private departmentService: DepartmentService,
              public ngxSmartModalService: NgxSmartModalService
    ,private vendorService: VendorService) {
    this.departmentForm = fb.group({
      'vendor': ['', Validators.compose([Validators.required])],
      'name': ['', Validators.compose([Validators.required])],
      'status': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.vendorService.findAll().subscribe((vendors) => {
      this.vendors = vendors;
    });
  }

  ngOnChanges(){
    if(this.department == undefined){
      this.department = new Department();
    }
  }

  save(){
    this.departmentService.save(this.department).subscribe((department)=>{
      this.departmentChange.emit(department);
      this.ngxSmartModalService.getModal('departmentModal').close()
    });
  }
}
