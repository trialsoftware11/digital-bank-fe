import { Component, OnInit } from '@angular/core';
import {DepartmentService} from "../../../services/department.service";
import {Department} from "../../../models/department.model";
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  departmentToDelete: Department;
  departmentToEdit: Department;
  action: string;
  departments: Array<Department> = [];

  constructor(public ngxSmartModalService: NgxSmartModalService,
              private departmentService: DepartmentService) { }

  ngOnInit() {
    this.departmentService.findAll().subscribe((departments) => {
      this.departments = departments;
    });
  }

  showDepartmentDialog(action: string, department: Department){
    this.departmentToEdit = department;
    this.action = action;
    this.ngxSmartModalService.getModal('departmentModal').open();
  }

  confirmDelete(department: Department){
    this.departmentToDelete = department;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.departmentService.delete(this.departmentToDelete.id).subscribe(()=>{
      this.departments = this.departments.filter((department) => {
        return department.id != this.departmentToDelete.id;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateDepartmentList(department: Department){
    if(!this.departments.some(elem => elem.id === department.id)){
      this.departments.push(department);
    }
    this.departmentToEdit = new Department();
  }

  lowerCase(input: string): string {
    return (input !== undefined)?input.toLowerCase():"";
  }

}
