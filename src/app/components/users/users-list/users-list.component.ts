import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../models/user.model";
import {NgxSmartModalService} from 'ngx-smart-modal';
import {VendorService} from "../../../services/vendor.service";
import {Vendor} from "../../../models/vendor.model";
import {DepartmentService} from "../../../services/department.service";
import {Department} from "../../../models/department.model";
import {UserRole} from "../../../enums/user.role";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users:Array<User> = [];
  userToDelete:User;
  userToEdit:User;
  action:string;
  vendors:Array<Vendor> = [];
  departments:Array<Department> = [];
  selectedUser:User;
  public userRoles:any = UserRole;

  constructor(private userService:UserService, public ngxSmartModalService:NgxSmartModalService,
              private vendorService:VendorService, private departmentService:DepartmentService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.findAll();
    this.vendorService.findAll().subscribe((vendors) => {
      this.vendors = vendors;
    });

    this.departmentService.findAll().subscribe((departments) => {
      this.departments = departments;
    });
  }

  findAll() {
    const currentUser = this.authService.user;

    if (currentUser.role == "SUPER_USER") {
      this.userService.findAll().subscribe((userList)=> {
        this.users = userList;
      });
    } else if (currentUser.role == "VENDOR_ADMIN") {
      this.userService.findByVendorId(currentUser.vendorId).subscribe((userList)=> {
        this.users = userList;
      });
    } else if (currentUser.role == "DEPT_ADMIN") {
      this.userService.findByDepartmentId(currentUser.departmentId).subscribe((userList)=> {
        this.users = userList;
      });
    }
  }

  showUserDialog(action:string, user:User) {
    this.userToEdit = user;
    this.action = action;
    this.ngxSmartModalService.getModal('userModal').open();
  }

  confirmDelete(user:User) {
    this.userToDelete = user;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete() {
    this.userService.delete(this.userToDelete.id).subscribe(()=> {
      this.users = this.users.filter((user) => {
        return user.id != this.userToDelete.id;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateUserList(user:User) {
    if (!this.users.some(elem => elem.id === user.id)) {
      this.users.push(user);
    }
    this.userToEdit = new User();
  }

  getVendorName(id:number) {
    let vendor = this.vendors.filter((v)=> {
      return v.id == id
    });
    if (vendor != undefined && vendor.length > 0) {
      return vendor[0].name
    }
  }

  getDepartmentName(id:number) {
    let department = this.departments.filter((v)=> {
      return v.id == id
    });
    if (department != undefined && department.length > 0) {
      return department[0].name
    }
  }

  showVendorDgl(user:User) {
    this.selectedUser = user;
    this.ngxSmartModalService.getModal('vendorDeptModal').open()
  }

  getClass(role) {
    if (this.userRoles[role] == 'SA' || this.userRoles[role] == 'SU') {
      return 'text-danger';
    } else if (this.userRoles[role] == 'VA') {
      return 'text-info';
    } else if (this.userRoles[role] == 'CU') {
      return 'text-success';
    } else {
      return 'text-warning';
    }
  }
}
