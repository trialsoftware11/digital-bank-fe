import { Component, OnInit,Input, EventEmitter,OnChanges, Output } from '@angular/core';
import {User} from "../../../models/user.model";
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import {ValidatePassword} from "../../../validators/password.validator";
import {Vendor} from "../../../models/vendor.model";
import {Department} from "../../../models/department.model";
import {VendorService} from "../../../services/vendor.service";
import {UserService} from "../../../services/user.service";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {UserRole} from "../../../enums/user.role";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnChanges {

  @Input()
  user: User = new User();

  public userForm:FormGroup;

  @Input()
  action: string = "Add";

  passwordConfirm:string;

  vendors: Array<Vendor> = [];
  departments: Array<Department> = [];

  @Output()
  userChange = new EventEmitter();

  roleNames: string[] = [];

  constructor(private fb:FormBuilder,private vendorService: VendorService,
  private userService: UserService,public ngxSmartModalService: NgxSmartModalService,
  private authService: AuthService) {
    this.userForm = fb.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'lastName': ['', Validators.compose([Validators.required])],
      'gender': ['', Validators.compose([Validators.required])],
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'vendor': ['', Validators.compose([Validators.required])],
      'status': [''],
      'role':[''],
      'department': [''],
      'username': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      'passwordConfirm': ['', Validators.compose([Validators.required, ValidatePassword])],
    });
  }

  ngOnInit() {
    for(var n in UserRole) {
      if(this.currentUser() != undefined && this.currentUser().role == "SUPER_USER" && n == "VENDOR_ADMIN") {
        this.roleNames.push(n);
      }

      if(this.currentUser() != undefined && this.currentUser().role == "VENDOR_ADMIN" &&
        (n == "VENDOR_ADMIN" || n == "DEPT_ADMIN" || n == "DEPT_USER")) {
        this.roleNames.push(n)
      }

      if(this.currentUser() != undefined && this.currentUser().role == "DEPT_ADMIN" &&
        (n == "DEPT_ADMIN" || n == "DEPT_USER" || n == "CLIENT_USER")) {
        this.roleNames.push(n)
      }
    }
  }

  loadDepartments(){
    let vendor = this.vendors.filter((v) => {
      return v.id == this.user.vendorId
    });

    if(vendor != undefined && vendor.length > 0){
      this.departments = vendor[0].departments;
    }
  }

  ngOnChanges(){
    if(this.user!==undefined){
      this.passwordConfirm = this.user.password;
    }else {
      this.user = new User();
    }

    this.vendorService.findAll().subscribe((vendors) => {
      this.vendors = vendors;
      this.loadDepartments();
    });

  }

  currentUser(){
    return this.authService.user;
  }

  save(){
    if(this.user.role == "CLIENT_USER"){
      this.user.departmentId = 0;
      this.user.vendorId = 0;
    }
    this.userService.save(this.user).subscribe((user)=>{
      this.userChange.emit(user);
      this.ngxSmartModalService.getModal('userModal').close()
    });
  }

}
