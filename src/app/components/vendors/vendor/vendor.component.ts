import { Component, OnInit, EventEmitter,OnChanges } from '@angular/core';
import {Vendor} from "../../../models/vendor.model";
import {Input} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {FormGroup} from "@angular/forms";
import {Validators} from "@angular/forms";
import {Output} from "@angular/core";
import {VendorService} from "../../../services/vendor.service";
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit, OnChanges {

  @Input()
  vendor: Vendor = new Vendor();

  public vendorForm:FormGroup;

  @Input()
  action: string = "Add";

  @Output()
  vendorChange = new EventEmitter();

  constructor(private fb:FormBuilder,private vendorService: VendorService,
              public ngxSmartModalService: NgxSmartModalService) {
    this.vendorForm = fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'status': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {

  }

  ngOnChanges(){
    if(this.vendor == undefined){
      this.vendor = new Vendor();
    }
  }

  save(){
    this.vendorService.save(this.vendor).subscribe((vendor)=>{
      this.vendorChange.emit(vendor);
      this.ngxSmartModalService.getModal('vendorModal').close()
    });
  }


}
