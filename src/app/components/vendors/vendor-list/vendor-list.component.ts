import {Component, OnInit} from "@angular/core";
import {Vendor} from "../../../models/vendor.model";
import {Department} from "../../../models/department.model";
import { NgxSmartModalService } from 'ngx-smart-modal';
import {VendorService} from "../../../services/vendor.service";

@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.component.html',
  styleUrls: ['./vendor-list.component.css']
})
export class VendorListComponent implements OnInit {
  vendorToDelete: Vendor;
  vendorToEdit: Vendor;
  action: string;
  vendors: Array<Vendor> = [];
  departments: Array<Department> = [];

  constructor(public ngxSmartModalService: NgxSmartModalService,
              private vendorService: VendorService) { }

  ngOnInit() {
    this.vendorService.findAll().subscribe((vendors) => {
      this.vendors = vendors;
    });
  }

  showVendorDialog(action: string, vendor: Vendor){
    this.vendorToEdit = vendor;
    this.action = action;
    this.ngxSmartModalService.getModal('vendorModal').open();
  }

  confirmDelete(vendor: Vendor){
    this.vendorToDelete = vendor;
    this.ngxSmartModalService.getModal('deleteModal').open()
  }

  delete(){
    this.vendorService.delete(this.vendorToDelete.id).subscribe(()=>{
      this.vendors = this.vendors.filter((vendor) => {
        return vendor.id != this.vendorToDelete.id;
      })
      this.ngxSmartModalService.getModal('deleteModal').close()
    });
  }

  updateVendorList(vendor: Vendor){
    if(!this.vendors.some(elem => elem.id === vendor.id)){
      this.vendors.push(vendor);
    }
    this.vendorToEdit = new Vendor();
  }

  lowerCase(input: string): string {
    return (input !== undefined)?input.toLowerCase():"";
  }

}
