import {Component} from "@angular/core";
import {AuthService} from "./services/auth.service";
import {Router} from "@angular/router";
import {UserRole} from "./enums/user.role";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  currentDate = new Date();

  constructor(private authService: AuthService, private router: Router){

  }

  isAuthenticated() {
     return this.authService.isAuthenticated();
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/login'])
  }

  navigateTo(url: string){
    this.router.navigate(['/'+url]);
  }

  currentUser(){
    return (this.authService.user != undefined)?this.authService.user:{
      firstName: "Demo",
      lastName: "User"
    };
  }

  currentVendor() {
    return (this.authService.userVendor != undefined)?this.authService.userVendor:{
      name: "DemoVendor"
    };
  }
}
