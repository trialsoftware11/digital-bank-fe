/**
 * Created by lindikhaya on 12/16/19.
 */
export class Customer {
  public customerId:number;
  public clientUserId:number;
  public firstName:string;
  public lastName;
  public male:boolean;
  public female:boolean;
  public married:boolean;
  public disability:boolean;
  public selfEmployed:boolean;
  public lifeInsured:boolean;
  public medicalAid:boolean;
  public pension:boolean;
  public salaryOnDebitOrder:number;
  public dateOfBirth: string;
  public age:number;
  public positiveBalanceScore:number;
  public increasingBalanceScore:number;
  public status:string;
  public gender:string;

  constructor(firstName: string, lastName: string){
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
