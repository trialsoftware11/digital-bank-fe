import {Vendor} from "./vendor.model";
export class User {
  public id: number;
  public firstName: string;
  public lastName: string;
  public gender: string;
  public username: string;
  public password: string;
  public email: string;
  public role: string;
  public status: string;
  public vendorId: number;
  public departmentId: number;
  public forgotPasswordToken: string;
  public forgotPasswordTokenExpiry: string;
}
