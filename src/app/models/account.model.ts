/**
 * Created by lindikhaya on 12/16/19.
 */
import {Customer} from "./customer.model";
import {AccountType} from "../enums/account.type";

export class Account {

  public accountId:number;
  public accountType:AccountType;
  public ownerId:number;
  public minimumBalance:number;
  public balance:number;
  public lastBalance:number;
  public score:number;
  public withdrawalRestriction:number;
  public withdrawalLimit:number;
  public overdrawLimit:number;
  public accrualRate:number;
  public status:string;
  public owner:Customer;

  constructor(accountId: number, owner: Customer, score: number){
    this.accountId = accountId;
    this.owner = owner;
    this.score = score;
  }

}
