/**
 * Created by lindikhaya on 12/16/19.
 */

export class Command {

  public commandId:number;
  public instructionId:string;
  public name:string;
  public commandSource:string;
  public commandCreationDate:Date;
  public userInitiating:string;
  public accountId:number;
  public assetId:number;
  public status:string

}
