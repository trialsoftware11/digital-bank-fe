/**
 * Created by lindikhaya on 12/14/19.
 */
export class Event {
  public eventId:number;
  public accountId:number;
  public assetId:number;
  public userId:number;
  public type:string;
  public title:string;
  public description:string;
  public amount:number;
  public transactionType:string;
  public transactionId:number;
  public creationDate:Date = new Date()

  constructor(creationDate: Date, type: string, transactionType){
    this.creationDate = creationDate;
    this.type = type;
    this.transactionType = transactionType;

  }
}
