/**
 * Created by lindikhaya on 12/16/19.
 */
export class Liability {
  public name:string;
  public currentValue:number;

  constructor(name: string, currentValue: number){
    this.name = name;
    this.currentValue = currentValue;
  }
}
