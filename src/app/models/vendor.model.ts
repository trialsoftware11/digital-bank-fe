import {Department} from "./department.model";
import {Attribute} from "../decorators/attribute.decorator";

export class Vendor {
  public id: number;

  @Attribute({
    name: 'name',
    label: 'Name'
  })
  public name: string;

  @Attribute({
    name: 'status',
    label: 'Status'
  })
  public status: string;
  public departments: Array<Department> = [];

  
}
