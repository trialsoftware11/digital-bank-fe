/**
 * Created by lindikhaya on 12/16/19.
 */
export class Asset {
  public assetId:number;
  public assetType:string;
  public ownerId:number;
  public name:string;
  public initialValue:number;
  public valueOwing:number;
  public currentValue:number;
  public marketValue:number;
  public dateOfPurchase:Date;
  public marketStatus:number;
  public status:string;

  constructor(name: string, currentValue: number){
    this.name = name;
    this.currentValue = currentValue;
  }
}
