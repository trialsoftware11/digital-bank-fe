/**
 * Created by lindikhaya on 12/14/19.
 */

export class Transaction {
  public transactionId:number;
  public name:string;
  public transactionValueType:string
  public targetClassName:string
  public targetClassFieldName:string
  public sourceClassName:string
  public sourceClassFieldName:string
  public sourceValue:string

  constructor(name: string,transactionValueType: string, sourceClassFieldName:string,
              sourceValue: string){
    this.name = name;
    this.transactionValueType = transactionValueType;
    this.sourceClassFieldName = sourceClassFieldName;
    this.sourceValue = sourceValue;
  }

}
