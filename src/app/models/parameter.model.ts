/**
 * Created by lindikhaya on 12/16/19.
 */
import {Customer} from "./customer.model";
import {AccountType} from "../enums/account.type";

export class Parameter {

  public parameterId:number;
  public actionId:number;
  public parameterName:string;
  public parameterFieldName:string;
  public parameterValue:string;
  public parameterValueType:string;
  public filterCondition:string;
  public nextJoinType:string;
  public status:string;

}
