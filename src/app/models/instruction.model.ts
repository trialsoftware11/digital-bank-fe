/**
 * Created by lindikhaya on 12/16/19.
 */
export class Instruction {

  public instructionId:number;
  public name:number;
  public notes:number;
  public instructionType:number;
  public initiatingUserId:number;
  public executingUserId:number;
  public approvalUserId:number;
  public approvalStatus:number;
  public executionStatus:number;
  public instructionCreationDate:Date;
  public instructionExecutionDate:Date;
  public status:number

}
