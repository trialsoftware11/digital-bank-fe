/**
 * Created by lindikhaya on 12/18/19.
 */
export class Dummy {
  savings : number;
  current: number;
  totalCash : number;
  callAccount : number;
  unitTrust : number;
  totalInvestments: number;
  vehicleLoan : number;
  personalLoan : number;
  totalLoans : number;
  residentialLoan: number;
  totalMortgages: number;
}
