/**
 * Created by lindikhaya on 12/9/19.
 */
import "reflect-metadata";

interface IAttributeProperties {
  name?: string;
  label?: string;
  type?: string;
  isEditable?: boolean;
  isVisible?: boolean;
}

export const ATTRIBUTE_PREFIX = 'attribute:';

/**
 * Adds attribute metadata to a property
 * @param {IAttributeProperties} attributes
 * @returns {(target: any, propertyKey: string) => void}
 * @constructor
 */
export function Attribute(attributes: IAttributeProperties) {
  return (target: object, propertyKey: string) => {
    if (attributes !== undefined && attributes !== null) {
      Object.keys(attributes).forEach(key => {
        // console.log(`${ATTRIBUTE_PREFIX}${key}`);
        // console.log(target.constructor.name+" "+propertyKey);
        Reflect.defineMetadata(`${propertyKey}:${ATTRIBUTE_PREFIX}${key}`, attributes[key], target/*, propertyKey*/);
      });
    }
  };
}
