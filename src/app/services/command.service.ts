/**
 * Created by lindikhaya on 12/19/19.
 */
import {Injectable} from "@angular/core";

import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";
import {Command} from "../models/command.model";

@Injectable()
export class CommandService extends AbstractService<Command>{
  constructor(http:HttpClient) {
    super("command",http);
  }
}
