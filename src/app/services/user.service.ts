import {Injectable, isDevMode} from "@angular/core";
import {User} from "../models/user.model";
import {HttpHeaders, HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {catchError, tap} from "rxjs/operators";
import {of} from "rxjs/index";
import { environment } from '../../environments/environment';
import { String } from 'typescript-string-operations';

@Injectable()
export class UserService {
  const
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  user:User;

  constructor(private http:HttpClient) {
  }

  register(user:User):Observable<any[]  | User> {
    return this.http.post<User>(environment.registerUser, JSON.stringify(user), this.httpOptions) .pipe(
      catchError(this.handleError('authenticateUser', []))
    );
  }

  findByToken(token:string):Observable<any | User> {
    return this.http.get<User>(environment.findUserByToken+ "/" + token) .pipe(
      catchError(this.handleError('findUserByToken', []))
    );
  }

  save(user:User):Observable<any[]  | User> {
    return this.http.post<User>(environment.saveUser, JSON.stringify(user), this.httpOptions) .pipe(
      catchError(this.handleError('saveUser', []))
    );
  }

  sendPasswordResetEmail(user:User):Observable<any[]  | User>  {
    return this.http.post<User>(environment.forgotPassword, JSON.stringify(user), this.httpOptions) .pipe(
      catchError(this.handleError('authenticateUser', []))
    );
  }

  activate(token:string):Observable<any | User> {
    return this.http.get<User>(environment.activateUser + "/" + token) .pipe(
      catchError(this.handleError('activate', []))
    );
  }

  findAll():Observable<User[]> {
    return this.http.get<User[]>(environment.findAllUsers) .pipe(
      catchError(this.handleError('findAllUsers', []))
    );
  }

  findByVendorId(vendorId: number):Observable<User[]> {
    return this.http.get<User[]>(String.Format(environment.findByVendorId,vendorId)) .pipe(
      catchError(this.handleError('findByVendorId', []))
    );
  }

  findByDepartmentId(departmentId: number):Observable<User[]> {
    return this.http.get<User[]>(String.Format(environment.findByDepartmentId, departmentId)) .pipe(
      catchError(this.handleError('findByDepartmentId', []))
    );
  }

  delete(id: number):Observable<string | any> {
    return this.http.get<string>(environment.deleteUser+"/"+id).pipe(
      catchError(this.handleError('deleteUser', []))
    );
  }

  private handleError<T>(operation = 'operation', result?:T) {
    return (error:any):Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
