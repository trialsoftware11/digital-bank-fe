import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {Vendor} from "../models/vendor.model";
import { catchError } from 'rxjs/operators';
import {isDevMode} from "@angular/core";
import {of} from "rxjs/index";
import { environment } from '../../environments/environment';

@Injectable()
export class VendorService {

  const
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http:HttpClient) {
  }

  findAll():Observable<Vendor[]> {
    return this.http.get<Vendor[]>(environment.allVendors) .pipe(
      catchError(this.handleError('getAllVendors', []))
    );
  }

  findById(id: number):Observable<any[] | Vendor> {
    return this.http.get<Vendor>(environment.findVendorById+id) .pipe(
      catchError(this.handleError('findById', []))
    );
  }

  save(vendor:Vendor):Observable<any[]  | Vendor> {
    return this.http.post<Vendor>(environment.saveVendor, JSON.stringify(vendor), this.httpOptions) .pipe(
      catchError(this.handleError('saveVendor', []))
    );
  }

  delete(id: number):Observable<string | any> {
    return this.http.get<string>(environment.deleteVendor+"/"+id).pipe(
      catchError(this.handleError('deleteUser', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
