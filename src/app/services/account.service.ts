import {Injectable} from "@angular/core";

import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";
import {Account} from "../models/account.model";
import {Observable} from "rxjs/index";
import {environment} from "../../environments/environment";
import { catchError } from 'rxjs/operators';
import { String } from 'typescript-string-operations';


@Injectable()
export class AccountService extends AbstractService<Account>{

  constructor(http:HttpClient) {
    super("account",http);
  }

  findByOwnerId(ownerId: number):Observable<Account[]> {
    return this.http.get<Account[]>(String.Format(environment.baseUrl,this.model,"findByOwnerId/"+ownerId)) .pipe(
      catchError(this.handleError('findByOwnerId', []))
    );
  }
}
