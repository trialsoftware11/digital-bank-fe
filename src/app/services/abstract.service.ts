/**
 * Created by lindikhaya on 12/8/19.
 */
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/index";
import { catchError } from 'rxjs/operators';
import {of} from "rxjs/index";
import { environment } from '../../environments/environment';
import { String, StringBuilder } from 'typescript-string-operations';

export class AbstractService<T> {

  const
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  model: string;

  constructor(model: string, public http:HttpClient) {
    this.model = model;
  }

  findAll():Observable<T[]> {
    return this.http.get<T[]>(String.Format(environment.baseUrl,this.model,"findAll")) .pipe(
      catchError(this.handleError('getAll', []))
    );
  }


  findById(id: number):Observable<any | T> {
    return this.http.get<T>(String.Format(environment.baseUrl,this.model,"findById/"+id)) .pipe(
      catchError(this.handleError('findById', []))
    );
  }

  save(t:T):Observable<any[]  | T> {
    return this.http.post<T>(String.Format(environment.baseUrl,this.model,"save"), JSON.stringify(t), this.httpOptions) .pipe(
      catchError(this.handleError('save', []))
    );
  }

  delete(id: number):Observable<string | any> {
    return this.http.get<string>(String.Format(environment.baseUrl,this.model,"delete"+"/"+id)).pipe(
      catchError(this.handleError('delete', []))
    );
  }

  public handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
