/**
 * Created by lindikhaya on 12/19/19.
 */
import {Injectable} from "@angular/core";

import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";
import {Parameter} from "../models/parameter.model";

@Injectable()
export class ParameterService extends AbstractService<Parameter>{
  constructor(http:HttpClient) {
    super("parameter",http);
  }
}
