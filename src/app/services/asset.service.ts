/**
 * Created by lindikhaya on 12/19/19.
 */
import {Injectable} from "@angular/core";

import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";
import {Asset} from "../models/asset.model";

@Injectable()
export class AssetService extends AbstractService<Asset>{
  constructor(http:HttpClient) {
    super("asset",http);
  }
}
