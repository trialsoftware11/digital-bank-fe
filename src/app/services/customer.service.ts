/**
 * Created by lindikhaya on 12/19/19.
 */
import {Injectable} from "@angular/core";

import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";
import {Customer} from "../models/customer.model";

@Injectable()
export class CustomerService extends AbstractService<Customer>{

  constructor(http:HttpClient) {
    super("customer",http);
  }

}
