import {Injectable} from "@angular/core";

import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";
import {Transaction} from "../models/transaction.model";
import { catchError } from 'rxjs/operators';
import { String } from 'typescript-string-operations';
import {Observable} from "rxjs/index";
import {environment} from "../../environments/environment";
import {Dummy} from "../models/dummy.model";

@Injectable()
export class TransactionService extends AbstractService<Transaction>{

  constructor(http:HttpClient) {
    super("transaction",http);
  }

  getDummy():Observable<any | Dummy> {
    return this.http.get<Dummy>(String.Format(environment.baseUrl,this.model,"findDummyTransactions")) .pipe(
      catchError(this.handleError('getAll', []))
    );
  }

}
