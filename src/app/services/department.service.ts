import {Injectable} from "@angular/core";

import {Department} from "../models/department.model";
import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";


@Injectable()
export class DepartmentService extends AbstractService<Department>{

  constructor(http:HttpClient) {
    super("department",http);
  }


}
