/**
 * Created by lindikhaya on 12/19/19.
 */
import {Injectable} from "@angular/core";

import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";
import {Instruction} from "../models/instruction.model";

@Injectable()
export class InstructionService extends AbstractService<Instruction>{
  constructor(http:HttpClient) {
    super("instruction",http);
  }
}
