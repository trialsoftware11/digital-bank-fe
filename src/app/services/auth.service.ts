import {isDevMode} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {catchError, map, tap} from 'rxjs/operators';
import {HttpHeaders, HttpClient} from "@angular/common/http";
import {User} from "../models/user.model";
import {of} from "rxjs/index";
import {Injectable} from "@angular/core";
import {Vendor} from "../models/vendor.model";
import {Department} from "../models/department.model";
import { environment } from '../../environments/environment';
@Injectable()
export class AuthService {

  const
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  user: User;
  authenticated = false;
  userVendor: Vendor;

  constructor(private http:HttpClient) {
  }

  authenticate(user: User):Observable<User[]> {
    return this.http.post<User[]>(environment.authenticate, JSON.stringify(user), this.httpOptions) .pipe(
      tap(users =>
        this.setAuthenticated(users)
      ),
      catchError(this.handleError('authenticateUser', []))
    );
  }

  setAuthenticated(users: User[]){
    if(users != null && users['id'] > 0){
      this.user = users[0];
      this.authenticated = true;
    }
  }

  isAuthenticated(){
    return this.authenticated;
  }

  logout() {
    this.user = null;
    this.authenticated = false;
  }

  private handleError<T>(operation = 'operation', result?:T) {
    return (error:any):Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
