import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {LoginComponent} from "./components/login/login.component";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {ForgotPasswordComponent} from "./components/forgot-password/forgot-password.component";
import {RegisterComponent} from "./components/register/register.component";
import {AuthGuard} from "./guards/auth.guard";
import {ResetPasswordComponent} from "./components/reset-password/reset-password.component";
import {ActivateUserComponent} from "./components/activate-user/activate-user.component";
import {UsersListComponent} from "./components/users/users-list/users-list.component";
import {VendorListComponent} from "./components/vendors/vendor-list/vendor-list.component";
import {DepartmentListComponent} from "./components/departments/department-list/department-list.component";
import {CustomerListComponent} from "./components/customers/customer-list/customer-list.component";
import {TransactionsComponent} from "./components/transactions/transactions.component";
import {AccountListComponent} from "./components/accounts/account-list/account-list.component";
import {AssetListComponent} from "./components/assets/asset-list/asset-list.component";
import {InstructionListComponent} from "./components/instructions/instruction-list/instruction-list.component";
import {ParameterListComponent} from "./components/parameters/parameter-list/parameter-list.component";
import {CommandListComponent} from "./components/commands/command-list/command-list.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'reset-password', component: ResetPasswordComponent},
  { path: 'activate-user', component: ActivateUserComponent},
  { path: 'users-list', component: UsersListComponent, canActivate: [AuthGuard]},
  { path: 'vendor-list', component: VendorListComponent, canActivate: [AuthGuard]},
  { path: 'department-list', component: DepartmentListComponent, canActivate: [AuthGuard]},
  { path: 'customer-list', component: CustomerListComponent, canActivate: [AuthGuard]},
  { path: 'transactions', component: TransactionsComponent, canActivate: [AuthGuard]},
  { path: 'accounts', component: AccountListComponent, canActivate: [AuthGuard]},
  { path: 'assets', component: AssetListComponent, canActivate: [AuthGuard]},
  { path: 'instructions', component: InstructionListComponent, canActivate: [AuthGuard]},
  { path: 'commands', component: CommandListComponent, canActivate: [AuthGuard]},
  { path: 'parameters', component: ParameterListComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
