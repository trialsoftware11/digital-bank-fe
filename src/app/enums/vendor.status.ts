/**
 * Created by lindikhaya on 12/7/19.
 */
export enum VendorStatus {
  ACTIVE,
  SUSPENDED
}
