/**
 * Created by lindikhaya on 12/29/19.
 */

export enum UserRole {
  SUPER_USER = "SU",
  VENDOR_ADMIN = "VA",
  DEPT_ADMIN = "DA",
  DEPT_USER = "DU",
  CLIENT_USER = "CU"
}
