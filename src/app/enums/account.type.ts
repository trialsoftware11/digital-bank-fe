/**
 * Created by lindikhaya on 12/16/19.
 */
export enum AccountType {
  DEBIT = "DEBIT",
  CREDIT = "CREDIT",
  LOAN = "LOAN",
  INVESTMENT = "INVESTMENT"
}
