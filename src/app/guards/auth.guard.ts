import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {AuthService} from "../services/auth.service";

@Injectable(
  {
    providedIn: 'root',
  }
)
export class AuthGuard implements CanActivate {

  constructor(private router:Router, private authService:AuthService) {

  }

  canActivate(router:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }

}
