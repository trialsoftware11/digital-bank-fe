export const environment = {
  production: true,
  users: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users",
  authenticate: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/authenticate",
  allDepartments: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/department/getAll",
  registerUser: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/register",
  findUserByToken: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/findByToken",
  saveUser: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/save",
  forgotPassword: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/forgotPassword",
  activateUser: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/activate",
  findAllUsers: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/findAll",
  deleteUser: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/delete",

  allVendors:"http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/vendor/getAll",
  findVendorById:"http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/vendor/findById/",
  deleteVendor: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/vendor/delete",
  saveVendor: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/vendor/save",

  departmentEndpoint: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/department/",

  baseUrl: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/{0}/{1}",

  findByVendorId: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/findByVendorId/{0}",
  findByDepartmentId: "http://digitalbankbe.us-east-2.elasticbeanstalk.com/v1/users/findByDepartmentId/{0}",
};
