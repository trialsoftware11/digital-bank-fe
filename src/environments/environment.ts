// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  users: "http://192.168.8.103:8082/v1/users",
  authenticate: "http://192.168.8.103:8082/v1/users/authenticate",

  registerUser: "http://192.168.8.103:8082/v1/users/register",
  findUserByToken: "http://192.168.8.103:8082/v1/users/findByToken",
  saveUser: "http://192.168.8.103:8082/v1/users/save",
  forgotPassword: "http://192.168.8.103:8082/v1/users/forgotPassword",
  activateUser: "http://192.168.8.103:8082/v1/users/activate",
  findAllUsers: "http://192.168.8.103:8082/v1/users/findAll",
  deleteUser: "http://192.168.8.103:8082/v1/users/delete",

  //Vendor endpoints
  allVendors:"http://192.168.8.103:8082/v1/vendor/getAll",
  findVendorById:"http://192.168.8.103:8082/v1/vendor/findById/",
  deleteVendor: "http://192.168.8.103:8082/v1/vendor/delete",
  saveVendor: "http://192.168.8.103:8082/v1/vendor/save",

  departmentEndpoint: "http://192.168.8.103:8082/v1/department/",

  baseUrl: "http://192.168.8.103:8082/v1/{0}/{1}",


  findByVendorId: "http://192.168.8.103:8082/v1/users/findByVendorId/{0}",
  findByDepartmentId: "http://192.168.8.103:8082/v1/users/findByDepartmentId/{0}",
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
